
package menupapelaria;

import java.util.Scanner;

public class Papel extends Produto implements Manipulacao{
    private String cor;
    private String tipo;
    private float largura;
    private float altura;
    private int gramatura;
    private boolean paltado;

    

    public void Papel() {
       
    }

    public Papel(String cor, String tipo, float largura, float altura, int gramatura, boolean paltado, String marca, float valor) {
        super(marca, valor);
        this.cor = cor;
        this.tipo = tipo;
        this.largura = largura;
        this.altura = altura;
        this.gramatura = gramatura;
        this.paltado = paltado;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getLargura() {
        return largura;
    }

    public void setLargura(float largura) {
        this.largura = largura;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public int getGramatura() {
        return gramatura;
    }

    public void setGramatura(int gramatura) {
        this.gramatura = gramatura;
    }

    public boolean isPaltado() {
        return paltado;
    }

    public void setPaltado(boolean paltado) {
        this.paltado = paltado;
    }

    @Override
    public boolean cadastro() {
        Scanner leitura = new Scanner(System.in);
        System.out.println("Digite a cor do papel: ");
        String cor = leitura.nextLine(); 
        return false;
        
    }

    @Override
    public String consulta() {
        return "\nCor =" + this.cor + "\nTipo = " + this.tipo + "\nAltura = " + this.altura 
                + "\nGramatura = " + this.gramatura + "\nPaltado = " + this.paltado +
                "\nMarca = " + this.getMarca() + "\nValor = " + this.getValor()+"\n---------------------------"; 
    }
}

