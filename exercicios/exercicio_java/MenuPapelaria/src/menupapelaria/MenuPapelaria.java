package menupapelaria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Geovanna Fagundes de Oliveira
//Geovanna Gonçalves Gomes

public class MenuPapelaria {

    List<String> pedidos = new ArrayList<>();

    static void menu() {
        System.out.println(" ");
        System.out.println("--------- MENU ---------");
        System.out.println("x                      x");
        System.out.println("      (1) CADASTRAR     ");
        System.out.println("      (2) CONSULTAR     ");
        System.out.println("      (3) LISTAR TODOS  ");
        System.out.println("      (4) SAIR          ");
        System.out.println("x                      x");
        System.out.println("------------------------");
    }
    
    

    public void CadastrarPedido() {
        String resp;

        Scanner leitura = new Scanner(System.in);

        do {
            Pedido pedido = new Pedido();
            pedido.cadastro();
            pedido.calculaTotalPedido();
            pedidos.add(pedido.consulta());
           
            System.out.println(" ");
            System.out.println("Deseja fazer outro pedido? SIM/NAO");
            resp = leitura.nextLine();
            System.out.println(" ");
        } while (!"NAO".equals(resp));

    }
    
    public void Consultar(){
        Scanner leitura = new Scanner(System.in);
        System.out.println("");
        System.out.print("Qual pedido você deseja consultar?: ");
        int op = leitura.nextInt();
        
        if (op >= pedidos.size()){
            System.out.println("Pedido não encontrado!");   
        } else{
            System.out.println(pedidos.get(op));
        }
    }
    
    public void Listar(){
        for (int i = 0; i< pedidos.size();i++){
            System.out.println(pedidos.get(i));
        }
    }
    

    public static void main(String[] args) {

        String opcao;
        MenuPapelaria menu = new MenuPapelaria();

        do {
            menu();
            Scanner leitura = new Scanner(System.in);
            System.out.print("Digite uma opção: ");
            opcao = leitura.nextLine();

            switch (opcao) {
                case "1":
                    menu.CadastrarPedido();
                    //Existe alguns atributos que estão sendo lidos como FLOAT
                    //Os atributos Largura, Altura da Classe Papel 
                    //E o atributo Valor da Classe Papel, Caderno e CaixaLapis
                    //Dessa forma, na hora de colocar algum valor para esses atributos
                    //utilize a VÍRGULA, exemplo: valor: 10,50 
                    //Se colocar o ponto no lugar da vírgula, o código da ERRO!
                    //*OBS: O atributo Gramatura da Classe Papel está sendo lido como INTEIRO
                    //então não coloque vírgula!
                    break;
                    
                case "2":
                    menu.Consultar();
                    
                    break;
                    
                case "3":
                    menu.Listar();
                    break;
            }
                

        } while (!"4".equals(opcao));

    }

}
