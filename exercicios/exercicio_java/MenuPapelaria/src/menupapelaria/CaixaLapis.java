
package menupapelaria;

public class CaixaLapis extends Produto implements Manipulacao{
    private int quantidade;
    private boolean colorido;
    
    public void CaixaLapis(){
        
    }

    public CaixaLapis(int quantidade, boolean colorido, String marca, float valor) {
        super(marca, valor);
        this.quantidade = quantidade;
        this.colorido = colorido;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isColorido() {
        return colorido;
    }

    public void setColorido(boolean colorido) {
        this.colorido = colorido;
    }

    @Override
    public boolean cadastro() {
        return false;
        
    }

    @Override
    public String consulta() {
        return "\nquantidade = " + quantidade + "\ncolorido = " 
                + colorido + "\nmarca = " + this.getMarca() + "\nvalor = " + this.getValor()+ "\n----------------";
        
    }

    
    
    
    
    
}
