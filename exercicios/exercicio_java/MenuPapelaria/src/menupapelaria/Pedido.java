
package menupapelaria;


import java.util.ArrayList;
import java.util.Scanner;

public class Pedido implements Manipulacao {
    private Data data;
    private Cliente cliente;
    private float totalpedido;
    private ArrayList caixalapis;
    private ArrayList papel;
    private ArrayList caderno;
    
    ArrayList<String> totpapel = new ArrayList<>();
    ArrayList<String> totcader = new ArrayList<>();
    ArrayList<String> totcxlapis = new ArrayList<>();
    
    public void Pedido(){
        
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public float getTotalpedido() {
        return totalpedido;
    }

    public void setTotalpedido(float totalpedido) {
        this.totalpedido = totalpedido;
    }

    public ArrayList getCaixalapis() {
        return caixalapis;
    }

    public void setCaixalapis(ArrayList caixalapis) {
        this.caixalapis = caixalapis;
    }

    public ArrayList getPapel() {
        return papel;
    }

    public void setPapel(ArrayList papel) {
        this.papel = papel;
    }

    public ArrayList getCaderno() {
        return caderno;
    }

    public void setCaderno(ArrayList caderno) {
        this.caderno = caderno;
    }

    @Override
    public boolean cadastro() {
        Pedido pedido = new Pedido();
        Scanner leitor = new Scanner(System.in);
      
        int dia, mes, ano;
        System.out.println("Digite a data");
        
        System.out.print("Dia: ");
        dia = leitor.nextInt();
        
        System.out.print("Mês: ");
        mes = leitor.nextInt();
        
        System.out.print("Ano: ");
        ano = leitor.nextInt();
        
        Data data = new Data(dia,mes,ano);
        setData(data);
        //--------------------------------------------//
        leitor.nextLine();
        
        System.out.println(" ");
        
        String nome, cpf, tel;
        System.out.println("Digite os dados do Cliente");
        
        System.out.print("Nome: ");
        nome = leitor.nextLine();
        
        System.out.print("CPF: ");
        cpf = leitor.nextLine();
        
        System.out.print("Telefone: ");
        tel = leitor.nextLine();
        
        Cliente cliente = new Cliente (nome,cpf,tel);
        setCliente(cliente);
        //--------------------------------------------------//
        
        System.out.println(" ");
        
        String resposta;
        int opcao;
        int qtde;
        float preco = 0;
               
        do{
        System.out.println("Qual produto você deseja cadastrar? ");
        System.out.println(" (1) PAPEL ");
        System.out.println(" (2) CADERNO");
        System.out.println(" (3) CAIXA LÁPIS");
        
            System.out.println("");
            System.out.print("Opção: ");
            opcao = leitor.nextInt();
            
            switch(opcao){
                case 1: 
                    System.out.println("Quantos papéis você deseja cadastrar? ");
                    qtde = leitor.nextInt();
                    System.out.println(" ");
                    for (int i = 1; i<=qtde; i++){
                        System.out.print("Cor: ");
                        leitor.nextLine();
                        String c = leitor.nextLine();
                        System.out.print("Tipo: ");
                        String t = leitor.nextLine();
                        System.out.print("Largura: ");
                        float l = leitor.nextFloat();
                        System.out.print("Altura: ");
                        float a = leitor.nextFloat();
                        System.out.print("Gramatura: ");
                        int g = leitor.nextInt();
                        System.out.print("Paltado? true(SIM)/false(NÃO): ");
                        boolean p = leitor.nextBoolean();
                        System.out.print("Marca: ");
                        leitor.nextLine();
                        String m = leitor.nextLine();
                        System.out.print("Valor: ");
                        float v = leitor.nextFloat();
                        
                        Papel papel = new Papel(c,t,l,a,g,p,m,v); 
                        preco+= papel.getValor();
                        totpapel.add(papel.consulta());
                        setPapel(totpapel);
                        
                    }
                    break;
                
                case 2:
                    System.out.println(" ");
                    System.out.println("Quantos cadernos você deseja cadastrar? ");
                    qtde = leitor.nextInt();
                    System.out.println(" ");
                    for (int i = 1; i<=qtde; i++){
                        System.out.print("Quantidade de folhas: ");
                        int q = leitor.nextInt();
                        System.out.print("Tamanho: ");
                        leitor.nextLine();
                        String t = leitor.nextLine();
                        System.out.print("Tipo: ");
                        String t1 = leitor.nextLine();
                        System.out.print("Capadura? true(POSSUI)/false(NÃO POSSUI): ");
                        boolean b = leitor.nextBoolean();
                        System.out.print("Marca: ");
                        leitor.nextLine();
                        String m = leitor.nextLine();
                        System.out.print("Valor: ");
                        float v = leitor.nextFloat();
                        
                        
                        Caderno caderno = new Caderno(q,t,t1,b,m,v);
                        preco += caderno.getValor();
                        totcader.add(caderno.consulta());
                        setCaderno(totcader);
                        
                    }
                    break;
                
                case 3:
                    System.out.println(" ");
                    System.out.println("Quantas caixa de lápis você deseja cadastrar? ");
                    qtde = leitor.nextInt();
                    System.out.println(" ");
                    
                    for (int i = 1; i<=qtde; i++){
                        System.out.print("Quantidade: ");
                        int q = leitor.nextInt();
                        System.out.print("Colorido? true(É COLORIDO)/false(NÃO É COLORIDO): ");
                        boolean c = leitor.nextBoolean(); 
                        System.out.print("Marca: ");
                        leitor.nextLine();
                        String m = leitor.nextLine();
                        System.out.print("Valor: ");
                        float v = leitor.nextFloat();
                        
                        CaixaLapis cxlapis = new CaixaLapis(q,c,m,v);
                        preco+= cxlapis.getValor();
                        totcxlapis.add(cxlapis.consulta());
                        setCaixalapis(totcxlapis);  
       
                    }
                
                    break;
                    
            }
            setTotalpedido(preco);
              
            System.out.println(" ");
            System.out.println("Deseja cadastrar outro produto?SIM/NAO ");
            leitor.nextLine();
            resposta = leitor.nextLine();
            
        }while (!"NAO".equals(resposta));
        
        
        
        return true;
    }

    public double calculaTotalPedido(){
        double total = (float) this.totalpedido + this.totalpedido*0.18;
        return total;


    }
    @Override
    public String consulta() {
  
        return "x-----------------------x"+"\nPedido: " + "\n" +this.data + "\n" + this.cliente +
               "\ntotalpedido= R$ "+ this.calculaTotalPedido()+ "\n "+"\ncaixalapis="
                + caixalapis +"\n "+ " \npapel=" + papel + 
                "\n "+"\ncaderno=" + caderno + "\nx-----------------------x";
        
    }
   
    
    
}
