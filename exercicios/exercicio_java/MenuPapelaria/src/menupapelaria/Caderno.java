
package menupapelaria;

public class Caderno extends Produto implements Manipulacao{
    private int qtdefolhas;
    private String tamanho;
    private String tipo;
    private boolean capadura; 
    
    public void Caderno(){
        
    }

    public Caderno(int qtdefolhas, String tamanho, String tipo, boolean capadura, String marca, float valor) {
        super(marca, valor);
        this.qtdefolhas = qtdefolhas;
        this.tamanho = tamanho;
        this.tipo = tipo;
        this.capadura = capadura;
    }

    public int getQtdefolhas() {
        return qtdefolhas;
    }

    public void setQtdefolhas(int qtdefolhas) {
        this.qtdefolhas = qtdefolhas;
    }

    public String getTamanho() {
        return tamanho;
    }

    public void setTamanho(String tamanho) {
        this.tamanho = tamanho;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isCapadura() {
        return capadura;
    }

    public void setCapadura(boolean capadura) {
        this.capadura = capadura;
    }

    @Override
    public boolean cadastro() {
        return false;
        
    }

    @Override
    public String consulta() {
        return  "\nqtdefolhas = " + qtdefolhas + ", \ntamanho = " + tamanho + ", \ntipo = " + tipo + 
                ", \ncapadura = " + capadura + "\nmarca = " + this.getMarca()+ "\nvalor= " 
                + this.getValor()+"\n----------------------------";        
      
        
    }

  
    
    
    
}


